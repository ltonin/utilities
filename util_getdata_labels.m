function [Labels, Events] = util_getdata_labels(evttyp, evtpos, evtdur, nsamples, datatype)
% [Labels, Events] = util_getdata_labels(evttyp, evtpos, evtdur, nsamples, datatype)
%
% Create logical labels vectors from input information (gdf-like style).
%
% Input:
%   evttyp      Ordered vector with events markers (gdf->h.EVENT.TYP)
%   evtpos      Ordered vector with events position (gdf->h.EVENT.POS)
%   evtdur      Ordered vector with events duration (gdf->h.EVENT.TYP)
%   nsamples    Number of samples in the original file
%   datatype    Type of events structure (ONLY gdf available)

    if nargin < 5
        datatype = 'gdf';
    end
    
    
    %% Input check
    if (strcmpi(datatype, 'gdf') == 0)
        error('chk:dtype', [datatype ' label conversion not implemented']);
    end
    
    if(length(evttyp) ~= length(evtpos))
        error('chk:input', 'evttyp, evtpos and evtdur must have the same length');
    end
    
    if(length(evtpos) ~= length(evtdur))
        error('chk:input', 'evttyp, evtpos and evtdur must have the same length');
    end


    Events      = unique(evttyp);
    NumEvents   = length(Events);
    Labels      = false(nsamples, NumEvents);
    
    for evtId = 1:NumEvents
        cevt = Events(evtId);
        idx  = find(evttyp == cevt);
        cstart = evtpos(idx);
        cstop  = cstart + evtdur(idx);
        clabels = false(nsamples, 1);
        
        for i = 1:length(cstart)   
            clabels(cstart(i):cstop(i)) = true;
        end
        
        Labels(:, evtId) = clabels;
        
    end
    
end