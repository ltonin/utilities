function [mask, labels] = whi_proc_laplacian_mask(montage, numchannels, varargin)
% [Mask, NumChans] = whi_proc_laplacian_mask(Montage, Labels, NumNeightbours [, MaskTyp])
%
% The function creates a laplacian mask according to the Montage provided
% as argument. Montage is a two dimensional matrix with the indexes of the
% channels and zero-padding for empty elements (see also:
% proc_get_montage). The function check the correct format of the Montage.
% NumNeightbours represents the number of neightbours to be considered 
% when the mask is created. The optional argument MaskTyp represents the 
% typology of mask to be created ('standard': all neightbours [default], 
% 'cross': excluding diagonal elements).
%
% Laplacian weights are computed by taking into account the number of
% neightbours of each channel.
%
% The function returns a square matrix (NumChans x NumChans) with the
% laplacian weights.
%
% SEE ALSO: proc_get_montage, proc_laplacian

    %% Input parser
    
    % Default patterns
    defaultMask   = 'cross';
    defaultLabels = {};
    
    % Validation functions
    ismasktype  = @(x) assert(ischar(x) && isequal(regexp(x, '\w+'), 1), 'masktyp must be a string (e.g., ''.cross'')') ;
    islabels    = @(x) assert(iscellstr(x), 'labels must be a cell of strings');
    
    % Add parameters
    p = inputParser;
    p.addRequired('montage');
    p.addRequired('numchannels');
    p.addParameter('masktyp', defaultMask, ismasktype);
    p.addParameter('labels', defaultLabels, islabels);
 
    
    % Parse input
    parse(p, montage, numchannels, varargin{:});
    masktyp = p.Results.masktyp;
    labels  = p.Results.labels;
    
    %% Main
    nrows  = size(montage, 1);
    ncols  = size(montage, 2);
    
    % Check for the montage (missing electrodes)
    warning('backtrace', 'off')
    missing = setxor(1:numchannels, sort(montage(montage > 0))');
    if isempty(missing) == false
        warning('chk:mtg', ['[' mfilename '] Mssing electrodes in the laplacian montage: ' num2str(missing)]);
    end
    warning('backtrace', 'on')
    
    
    % Iterate for each channel index (different from 0) and retrieve
    % neigthbours. Apply a submask to the selected neightbours according to
    % the mask type provided.
    mask = zeros(numchannels);
    
    for rId = 1:nrows
        for cId = 1:ncols
            cchan = montage(rId, cId);
            
            if cchan > 0
               selrows = max(1, rId-1):min(nrows, rId+1);
               selcols = max(1, cId-1):min(ncols, cId+1);
               submontage = montage(selrows, selcols);
               
               switch lower(masktyp)
                    case 'cross'
                        submask = true(size(submontage));
                        [i, j]  = find(submontage == cchan);
                        submask(i, :) = false; 
                        submask(:, j) = false;  
                        submontage(submask) = 0;
                    otherwise
                        error('chk:msktyp', ['[' mfilename '] Unknown mask type: ' masktyp]);
               end
                % Retrieve neightbours (excluding the channels itself and
                % the neightbours with id equal to 0)
                NbrsId  = setdiff(submontage(submontage>0), cchan);
                NumNbrs = length(NbrsId);
                
                % Populate the laplacian mask
                mask(cchan, cchan)  = 1;
                mask(NbrsId, cchan) = -1/NumNbrs;   
            end
        end
    end
    
%     for rId = 1:nrows
%         for cId = 1:ncols
%             cchan = montage(rId, cId);
%             if cchan > 0
%                 selrows = max(1, rId-NumNeightbours):min(nrows, rId+NumNeightbours);
%                 selcols = max(1, cId-NumNeightbours):min(ncols, cId+NumNeightbours);
%     
%                 submontage = montage(selrows, selcols);
%                 
%                 % Switch between submask types
%                 switch lower(masktyp)
%                     case 'standard'
%                         % do nothing
%                     case 'cross'
%                         submask = true(size(submontage));
%                         [i, j]  = find(submontage == cchan);
%                         submask(i, :) = false; 
%                         submask(:, j) = false;  
%                         submontage(submask) = 0;
%                     otherwise
%                         error('chk:msktyp', ['[' mfilename '] Unknown mask type: ' masktyp]);
%                 end
%                 
%                 % Retrieve neightbours (excluding the channels itself and
%                 % the neightbours with id equal to 0)
%                 NbrsId  = setdiff(submontage(submontage>0), cchan);
%                 NumNbrs = length(NbrsId);
%                 
%                 % Populate the laplacian mask
%                 mask(cchan, cchan)  = 1;
%                 mask(NbrsId, cchan) = -1/NumNbrs;           
%             end
%         end
%     end
end
