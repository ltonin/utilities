function [result, data, featfile] = util_getfeatures(bdfname, featdir)
% [result, data, featfile] = util_getfeatures(bdfname, featdir)
%
% This function loads a .mat file identified by [featdir bdffilename '.mat']
% It returns both the data loaded inside the .mat file and the file path

    data = [];
    result = 0;
    
    [~, featfile] = fileparts(bdfname);
    featfile = [featdir featfile '.mat'];
    
    if exist(featfile, 'file') > 0
        result = 1;
        data = load(featfile);
    end
        

end
