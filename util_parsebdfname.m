function info = util_parsebdfname(bdfname)
% info = util_parsebdfname(bdfname)
%
% This function parses a specific bdf filename (in a specific format) and 
% it returns a structure with the following fields:
%
%    info.subj
%    info.date
%    info.time
%    info.mod
%    info.block
%    info.task

    [~, name, ~] = fileparts(bdfname);
    
    [subj, date, time, modality, block, task] = ...
        strread(name, '%s%d%d%s%s%s', 'delimiter', '.');
    
    info.subj  = char(subj);
    info.date  = date;
    info.time  = time;
    info.mod   = char(modality);
    info.block = char(block);
    info.task  = char(task);

end
