function util_disp(s, format)

    if nargin == 1
        format = '';
    end
    
    if strcmpi(format, 'b')
        disp(['<strong>' s '</strong>']);
    else
        disp(s);
    end

    

end
