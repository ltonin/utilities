function analysis = util_load_analysis(dir, subject, pattern, verbose)
% analysis = util_load_analysis(dir, subject, [pattern, verbose])
%
% This function load a .mat file (considered as saved analysis) and it returns it
% as output. If the file .mat does not exist then a empty variable is created and is
% returned. The .mat file is identified by:
%
% analysis_file = [dir '/' subject '_' pattern '.mat']
%
% Default values:
% pattern = 'analysis';
% verbose = 1

    if nargin < 3
        pattern = 'analysis';
        verbose = 1;
    end
    
    if nargin < 4
        verbose = 1;
    end
    
    analysis_file = [dir '/' subject '_' pattern '.mat'];
    
    if exist(analysis_file, 'file') > 0
        if verbose
            util_bdisp(['[Input] - Loading ' pattern ' from: ' analysis_file]);
        end
        analysis = load(analysis_file);
    else
        if verbose
            util_bdisp(['[Input] - Loading empty ' pattern]);
        end
        analysis = [];
    end
end
