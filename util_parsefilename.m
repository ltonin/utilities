function fileinfo = util_parsefilename(filename)
% fileinfo = util_parsefilename(filename)
%
% Parse a filename in the format of CNBI loop:
% SUBJECT.DATE.TIME.MODALITY.BLOCK.TASK
%
% If the format is not recognize a warning occurs.

    [~, name] = fileparts(filename);
    cellentries = textscan(name, '%s', 'Delimiter', '.');
    entries = cellentries{1};
    
    if (length(entries) ~= 6) 
        warning('chk:name', 'Filename format is not recognized');
        fileinfo.subject  = '';
        fileinfo.date     = '';
        fileinfo.time     = '';
        fileinfo.modality = '';
        fileinfo.block    = '';
        fileinfo.task     = '';
    else
    
    
        fileinfo.subject  = entries{1};
        fileinfo.date     = str2double(entries{2});
        fileinfo.time     = str2double(entries{3});
        fileinfo.modality = entries{4};
        fileinfo.block    = entries{5};
        fileinfo.task     = entries{6};
    end
end