function [s, events, info] = util_cat_gdf(filepaths)
% [s, events] = util_cat_gdf(filepaths)
%
% The function concatanates signals and events information coming from
% different gdf files indicated in filepaths cell.
%
% Input:
%       filepaths       Cell array with path to GDF files
%
% Output:
%       s [samples x trials]    Concatenated signal
%       events.typ              Concatenated event markers
%       events.pos              Concatenated event positions
%       events.dur              Concatenated event durations
%       info.filepaths          Paths of the concatenated gdf files
%       info.datasize           Sizes of each concatenated gdf file
%       info.modality           0 or 1 according if in the filename there
%                               is the substring 'offline' or 'online'
%
% SEE ALSO: sload

    NumFiles = length(filepaths);
    s   = [];
    typ = [];
    dur = [];
    pos = [];
    
    datasize  = zeros(2, NumFiles);
    modality  = nan(NumFiles, 1);
    modlabels = {'offline', 'online'};
    
    for fId = 1:NumFiles
        cfile = filepaths{fId};
        [cs, ch] = sload(cfile);
        cs = cs(:, 1:end-1);

        typ = cat(1, typ, ch.EVENT.TYP);
        dur = cat(1, dur, ch.EVENT.DUR);
        pos = cat(1, pos, ch.EVENT.POS + size(s, 1) + 1);
        
        start = size(s, 1) + 1;
        stop  = start + size(cs, 1) - 1;
        datasize(:, fId) = [start stop];
        
        s = cat(1, s, cs);

        
        
        for mId = 1:length(modlabels)
            if(isempty(strfind(cfile, modlabels{mId})) == false)
                modality(fId) = mId - 1;
            end
        end
        
    end

    events.typ = typ;
    events.pos = pos;
    events.dur = dur;
    
    info.filepaths  = filepaths;
    info.datasize   = datasize;
    info.modality   = modality;
    info.modlabels  = modlabels;


end