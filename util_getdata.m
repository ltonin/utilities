function [files, folders] = util_getdata(datapath, dirpattern, filepattern, extension)
% [files, numfiles] = util_getdata(datapath, dirpattern, filepattern, extension)
%
% The function get files from structured directories. It looks for
% directories in DATAPATH that match DIRPATTERN. Then, for each directory
% founded, it looks for all files with EXTENSION that match FILEPATTERN.
% It return a cell array with all files found and the number of files
% found.
%
% SEE ALSO: util_getdir, util_getfile

    if isempty(dirpattern)
        dirpattern = '';
    end

    folders = util_getdir(datapath, dirpattern);
    nfolders = length(folders);
    
    files = {};
  
    for dId = 1:nfolders
        cfiles = util_getfile(folders{dId}, extension, filepattern);
        files = cat(1, files, cfiles);
    end
end