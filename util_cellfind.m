function [id, key] = util_cellfind(cellkey, cellarray)
% [id, key] = util_cellfind(cellkey, cellarray)
% 
% The function finds matches of cellkey in a cell array. It returns the
% identifier position of the matches in cellarray and the corresponding
% keys.
%
% SEE ALSO: strcmp

    nkeys = length(cellkey);
    
    id = [];
    for kId = 1:nkeys
        cid = find(strcmp(cellarray, cellkey{kId}), 1);
        if isempty(cid) == false
            id = cat(1, id, cid);
        end
    end

    key = cell(length(id), 1);
    if isempty(id) == false   
        for kId = 1:length(id)
            key{kId} = cellarray{id(kId)};
        end
    end

end