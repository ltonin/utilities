function values = whi_get_event(labels)
% WHI_GET_EVENT
% Given the name of the event, it returns the associated event value. The
% definition of the event is done by whi_init_events

    if iscell(labels) == false
        cell_labels{1} = labels;
    else
        cell_labels = labels;
    end

    list = whi_init_events;
    
    nevt = length(cell_labels);
    values = [];
    for eId = 1:nevt
        idx = find(cellfun(@(s) strcmpi(s{1}, cell_labels{eId}), list), 1);
        
        if isempty(idx)
            continue;
        end
        
        values = cat(1, values, list{idx}{2});
    end


end
