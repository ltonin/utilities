function [analysis] = util_save_analysis(analysis, dir, subject, pattern)
% [analysis] = util_save_analysis(analysis, dir, subject, pattern)
%
% This function saves the variable analysis in a given path. It returns the saved
% variable. The path has the following construction:
%
% analysis_file = [dir '/' subject '_' pattern '.mat']

    if nargin < 4
        pattern = 'analysis';
    end
    
    analysis_file = [dir '/' subject '_' pattern '.mat'];
    
    util_bdisp(['[Output] - Saving analysis in: ' analysis_file]);
    save(analysis_file, '-struct', 'analysis');

end
