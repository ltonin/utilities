function ext = util_getextension(filename)
% ext = util_getextension(filename)
%
% This function returns the extension of a given filename.

    [~, ~, ext] = fileparts(filename);
end
