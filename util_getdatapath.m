function datapath = util_getdatapath(pattern, envvar)
% datapath = util_getdatapath([pattern, envvar])
%
% This function retrieves the path indicated by the environment variable envvar and the
% the variable pattern. It creates the path followinf this rule: [ENVVAR_PATH / PATTERN]
%
% If no argument are provided, the default pattern is empty and the envvar = $VADATA_ROOT
% If only pattern is provided, the default envvar is $VADATA_ROOT
%
% The function check also the existence of the envvar provided as well as if the result path exist or not

    if nargin < 1
        pattern ='';
        envvar = 'CVSA_DATAROOT';
    end
   
    if nargin == 1
        envvar = 'CVSA_DATAROOT';
    end
    
    dir_var = getenv(envvar); 

    if isempty(dir_var)
        error('chk:env', [envvar ' environment variable is not set']);
    end

    
    
    datapath = [dir_var pattern];
    
    if exist(datapath, 'dir') ~= 7
        error('chk:path', ['The current path: ' datapath ' does not exist']);
    end
    
end
