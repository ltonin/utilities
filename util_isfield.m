function isFieldResult = util_isfield(inStruct, fieldName)
% isFieldResult = util_isfield(inStruct, fieldName)
%
% This function returns if fieldName is a field in structure inStruct. It check all 
% fields recursevely.

isFieldResult = 0;
f = fieldnames(inStruct(1));
for i=1:length(f)
    if(strcmp(f{i},strtrim(fieldName)))
        isFieldResult = 1;
        return;
    elseif isstruct(inStruct(1).(f{i}))
        isFieldResult = util_isfield(inStruct(1).(f{i}), fieldName);
        if isFieldResult
            return;
        end
    end
end
