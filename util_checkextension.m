function result = util_checkextension(filename, extension)
% result = util_checkextension(filename, extension)
%
% This function checks if a give filename has a given extension.
% It returns true or false accordingly.

    result = false;
    
    ext = util_getextension(filename);
    
    if strcmpi(ext, extension)
        result = true;
    end
end
