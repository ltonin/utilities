function list = whi_init_events
% WHI_INIT_EVENTS
% List of the default events

    list{1}  = {'both-hands',           773};
    list{2}  = {'both-feet',            771};
    list{3}  = {'continuous-feedback',  781};
    list{4}  = {'eog-on',               hex2dec('400')};
    list{5}  = {'eog-off',              hex2dec('8400')};
    list{6}  = {'command-left',         101};
    list{7}  = {'command-light',        102};
    list{8}  = {'command-right',        103};
    list{9}  = {'pad-left',             201};
    list{10} = {'pad-light',            202};
    list{11} = {'pad-right',            203};
    list{12} = {'pad-none',             204};
    list{13} = {'race-start',           800};
    list{14} = {'race-end',             8800};

end
