function files = util_getfile3(root, type, varargin)
% UTIL_GETFILE3 Get file with inclusion/exclusion criteria
%
%   files = UTIL_GETFILE3(root, type) returns all files in the directory 
%   root and in the subdirectories with the extension that match with 'type'.
%
%   files = UTIL_GETFILE3(root, type, 'include', {PATTERN1, PATTERN2, ...})
%   returns all files that match the provided patterns in the filename
%
%   files = UTIL_GETFILE3(root, type, 'exclude', {PATTERN1, PATTERN2, ...})
%   returns all files excluding the one matching the provided patterns in
%   the filename



    %% Input parser
    
    % Default patterns
    defaultInclude = {' '};
    defaultExclude = {' '};
    defaultLevel = 1;
    
    % Validation functions
    isextension = @(x) assert(ischar(x) && isequal(regexp(x, '\.\w+'), 1), 'type must be an extension (e.g., ''.gdf'')') ;
    isrootdir   = @(x) assert(isfolder(x), 'root must be a valid directory');
    ispattern   = @(x) assert(iscellstr(x) || ischar(x), 'inclusion/exclusion patterns must be a char array or a cell of chars');
    isnumber    = @(x) assert(isnumeric(x) && isscalar(x), 'level must be a scalar');
    
    % Add parameters
    p = inputParser;
    p.addRequired('root', isrootdir);
    p.addRequired('type', isextension);
    p.addParameter('include', defaultInclude, ispattern);
    p.addParameter('exclude', defaultExclude, ispattern);
    p.addParameter('level', defaultLevel, isnumber);
    
    % Parse input
    parse(p, root, type, varargin{:});
    
    % Convert patterns to cell array
    rule_include    = char2cell(p.Results.include);
    rule_exclude    = char2cell(p.Results.exclude);
    level           = p.Results.level;
    
    %% Get files (recurserverly)
    list = get_files(root, type, level);
    
    %% Include/Exclude patterns
    idx_include = include(list, rule_include);
    idx_exclude = exclude(list, rule_exclude);
    
    idx = idx_include & ~idx_exclude;
  
    %% Select valid files
    files = list(idx);
    nfiles = length(files);
    
    if(nfiles == 0)
        
        error(['[io] - No files found with the inclusion/exclusion criteria: (' strjoin(rule_include, ', ') ') / (' strjoin(rule_exclude, ', ') ')']);
    end
end

function files = get_files(root, type, level)

    % list entries in the given directory directory
    entries = dir(root);
    
    % remove '.' and '..' entries
    entries = entries(~ismember({entries.name}, {'.', '..'}));
    
    nentries = length(entries);
    
    files = {};
    
    for eId = 1:nentries
        centry = entries(eId);
        
        % If it is a dir, go recurseverly, otherwise concat filename 
        if (centry.isdir == true) && level > 1
            files = cat(1, files, get_files(fullfile(centry.folder, centry.name), type, level-1));
        else
            if matchextension(centry.name, type)
                files = cat(1, files, fullfile(centry.folder, centry.name));
            end
        end
        
    end
end

function idx = include(list, patterns)

    npatts = length(patterns);
    nfiles = length(list);
    idx  = true(nfiles, 1);
    
    for pId = 1:npatts
        idx = idx & contains(list, patterns{pId});
    end
    
end

function idx = exclude(list, patterns)
    npatts = length(patterns);
    nfiles = length(list);
    idx  = false(nfiles, 1);
    
    for pId = 1:npatts
        idx = idx | contains(list, patterns{pId});
    end
end

% function indices = excluded_dir(entries, patterns)
% 
%     nentry   = length(entries);
%     npattern = length(patterns);
%     
%     indices = false(nentry, 1);
%     
%     if isempty(patterns)
%         return;
%     end
%     
%     for lId = 1:nentry
%         cpath = fileparts(entries{lId});
%        
%         cexclude = false;
%         for pId = 1:npattern
%             cexclude = cexclude || matchpattern(cpath, patterns{pId});
%         end
%         
%         indices(lId) = cexclude;
%     end
% 
% end
% 
% function indices = included_filename(entries, patterns)
%  
%     nentry   = length(entries);
%     npattern = length(patterns);
%     
%     indices = true(nentry, 1);
%     
%     if isempty(patterns)
%         return;
%     end
%     
%     for lId = 1:nentry
%         [~, cname] = fileparts(entries{lId});
%         
%         cinclude = true;
%         for pId = 1:npattern
%             cinclude = cinclude && matchpattern(cname, patterns{pId});
%         end
%         
%         indices(lId) = cinclude;
%     end
% end
% 
% function indices = includeddisj_filename(entries, patterns)
%  
%     nentry   = length(entries);
%     npattern = length(patterns);
%     
%     indices = true(nentry, 1);
%     
%     if isempty(patterns)
%         return;
%     end
%     
%     for lId = 1:nentry
%         [~, cname] = fileparts(entries{lId});
%         
%         cinclude = false;
%         for pId = 1:npattern
%             cinclude = cinclude || matchpattern(cname, patterns{pId});
%         end
%         
%         indices(lId) = cinclude;
%     end
% end
% 
% function indices = excluded_filename(entries, patterns)
% 
%     nentry   = length(entries);
%     npattern = length(patterns);
%     
%     indices = false(nentry, 1);
%     
%     if isempty(patterns)
%         return;
%     end
%     
%     for lId = 1:nentry
%         [~, cname] = fileparts(entries{lId});
%         
%         cexclude = false;
%         for pId = 1:npattern
%             cexclude = cexclude || matchpattern(cname, patterns{pId});
%         end
%         
%         indices(lId) = cexclude;
%     end
% end
% 
% function matched = matchpattern(text, pattern)
%     matched = isempty(regexp(text, regexptranslate('wildcard', pattern), 'once')) == false;
% end

function matched = matchextension(filename, extension)
    matched = false;
    [~, ~, fileext] = fileparts(filename);
    if(strcmp(fileext, extension) == true)
        matched = true;
    end
end

function out = char2cell(in)
    
    out = in;
    if ischar(in)
        out{1} = in;
    end
end
