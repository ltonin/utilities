function [result, data, filename] = util_loadmat(name, dir)
% [result, data, file] = util_getfeatures(name, dir)
%
% This function loads a .mat file identified by [dir name '.mat']
% It returns both the data loaded inside the .mat file and the file path

    data = [];
    result = 0;
    
    [~, filename] = fileparts(name);
    filename = [dir filename '.mat'];
    
    if exist(filename, 'file') > 0
        result = 1;
        data = load(filename);
    end
        

end
